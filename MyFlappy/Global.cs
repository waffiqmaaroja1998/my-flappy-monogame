﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MyFlappy
{
    public class Global
    {
        public static GraphicsDeviceManager graphics;
        public static SpriteBatch spriteBatch;
    }
}
