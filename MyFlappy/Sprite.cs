﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MyFlappy
{
    public class Sprite
    {
        private Texture2D spriteTexture;
        public Rectangle spriteSource;
        public Rectangle spriteDestination;
        public Rectangle spriteBox;
        private Vector2 offsetBox;
        private float spriteRotation;

        public Sprite(Texture2D spriteTexture, Rectangle spriteSource, Rectangle spriteDestination, float rotation, Rectangle? spriteBox = null, Vector2? offsetBox = null)
        {
            this.spriteTexture = spriteTexture;
            this.spriteSource = spriteSource;
            this.spriteDestination = spriteDestination;
            this.spriteRotation = rotation;

            if (spriteBox.HasValue) this.spriteBox = spriteBox.Value;
            else this.spriteBox = this.spriteDestination;

            if (offsetBox.HasValue) this.offsetBox = offsetBox.Value;
            else this.offsetBox = Vector2.Zero;
        }

        public void Update(GameTime gameTime)
        {
            spriteBox.X = (int)(spriteDestination.X + offsetBox.X);
            spriteBox.Y = (int)(spriteDestination.Y + offsetBox.Y);
        }

        public void Draw(GameTime gameTime)
        {
            Global.spriteBatch.Draw(
                spriteTexture, 
                destinationRectangle: spriteDestination, 
                sourceRectangle : spriteSource, 
                color : Color.White, 
                rotation : spriteRotation);
        }
    }
}
