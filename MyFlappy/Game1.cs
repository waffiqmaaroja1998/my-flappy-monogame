﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace MyFlappy
{
    
    public class Game1 : Game
    {
        private int screenWidth;
        private int screenHeight;

        private int backgroundCount;
        private Sprite[] backgroundObject;

        private Texture2D background;
        private int currentBG, offsetBG;
        private float speedBackground;

        private int landCount;
        private Sprite[] landObject;

        private Texture2D land;
        private int currentLand, offsetLand;
        private float speedLand;

        //

        private Sprite characterObject;

        private Texture2D character;
        private Vector2 charaPosition;
        private Vector2 characterDimension;
        private Rectangle characterBox;
        private int whichCharacter;
        private float speedCharacter, speedOnJump, gravity;

        private Random random;
        private int obstacleCount;
        private int obstacleDistance, obstacleGap;
        private int firstObstacleIndex;
        private Sprite[,] obstacleObject;

        private Texture2D obstacle;
        private float obstacleSpeed;

        private Texture2D intersectBox;

        private enum GameState
        {
            Playing, Stop
        }
        private GameState currentState;

        public Game1()
        {
            Global.graphics = new GraphicsDeviceManager(this);
            screenWidth = Global.graphics.PreferredBackBufferWidth;
            screenHeight = Global.graphics.PreferredBackBufferHeight;
            
            currentState = GameState.Playing;
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            Global.spriteBatch = new SpriteBatch(GraphicsDevice);
            background = Content.Load<Texture2D>("Sprite/SkyTileSprite");
            land = Content.Load<Texture2D>("Sprite/GrassThinSprite");
            character = Content.Load<Texture2D>("Sprite/BirdHero");
            obstacle = Content.Load<Texture2D>("Sprite/ColumnSprite");
            intersectBox = Content.Load<Texture2D>("Sprite/Box");

            // BACKGROUND

            currentBG = 1;
            speedBackground = 100f;
            offsetBG = -100;

            backgroundCount = 3;
            backgroundObject = new Sprite[backgroundCount];
            for(int i=0; i<backgroundCount; i++)
            {
                backgroundObject[i] = new Sprite(
                    background,
                    new Rectangle(0, 0, background.Width, background.Height),
                    new Rectangle(screenWidth * i - screenWidth, 0, screenWidth, screenHeight),
                    0);
            }

            // LAND

            currentLand = 1;
            speedLand = 200f;
            offsetLand = -100;

            landCount = 4;
            landObject = new Sprite[landCount];

            Rectangle sourceLand = new Rectangle(0, land.Height / 2, land.Width, land.Height / 2);
            float ratioHeight = (sourceLand.Height / 50f);
            float ratioWidth = (sourceLand.Width / (float)screenWidth);
            float ratio = Math.Max(ratioHeight, ratioWidth);
            Vector2 landDimension = new Vector2((int)(sourceLand.Width / ratio), (int)(sourceLand.Height / ratio));
            for (int i = 0; i < landCount; i++)
            {
                landObject[i] = new Sprite(
                    land,
                    sourceLand,
                    new Rectangle((int)(landDimension.X * i - landDimension.X), (int)(screenHeight - landDimension.Y), (int)landDimension.X, (int)landDimension.Y),
                    0,
                    offsetBox : new Vector2(0, -10));
            }

            // CHARACTER

            whichCharacter = 0;
            gravity = 650f;
            speedCharacter = 0f;
            speedOnJump = -200f;

            charaPosition = new Vector2(100, 100);
            characterDimension = new Vector2(character.Width / 12, character.Height / 4);

            characterObject = new Sprite(
                character,
                new Rectangle(0, 0, character.Width / 3, character.Height),
                new Rectangle(100, 100, (int)characterDimension.X, (int)characterDimension.Y),
                0,
                new Rectangle(100, 100 + (int)characterDimension.X / 4, (int)characterDimension.X, (int)characterDimension.Y / 2),
                new Vector2(0, (int)characterDimension.X / 4));

            // OBSTACLE

            firstObstacleIndex = 0;
            obstacleSpeed = 150f;
            obstacleDistance = 150;
            obstacleGap = 100;
            obstacleCount = screenWidth / obstacleDistance + 2;
            obstacleObject = new Sprite[obstacleCount, 2];
            random = new Random();
            for (int i = 0; i < obstacleCount; i++)
            {
                int randY = random.Next(150, screenHeight - 150);
                obstacleObject[i, 0] = new Sprite(
                    obstacle,
                    new Rectangle(obstacle.Width / 3, 0, obstacle.Width / 3, obstacle.Height),
                    new Rectangle(
                        screenWidth + i * (obstacle.Width / 3 + obstacleDistance), 
                        randY, obstacle.Width / 3, obstacle.Height),
                    0);

                obstacleObject[i, 1] = new Sprite(
                    obstacle,
                    obstacleObject[i, 0].spriteSource,
                    new Rectangle(
                        screenWidth + i * (obstacle.Width / 3 + obstacleDistance) + obstacle.Width / 3, 
                        obstacleObject[i, 0].spriteDestination.Y - obstacleGap, obstacle.Width / 3, obstacle.Height),
                    (float)Math.PI,
                    offsetBox : new Vector2(0 - obstacle.Width / 3, 0 - obstacle.Height));
            }

            // TODO: use this.Content to load your game content here
        }

        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }
        
        private bool CollisionDetection(Rectangle recA, Rectangle recB)
        {
            //Vector2 intervalA_X = new Vector2(recA.X, recA.X + recA.Width);
            //Vector2 intervalB_X = new Vector2(recB.X, recA.X + recB.Width);
            //Vector2 intervalA_Y = new Vector2(recA.Y, recA.Y + recA.Height);
            //Vector2 intervalB_Y = new Vector2(recB.Y, recA.Y + recB.Height);
            //if (Math.Min(intervalA_X.Y, intervalB_X.Y) >= Math.Max(intervalA_X.X, intervalB_X.X) &&
            //    Math.Min(intervalA_Y.Y, intervalB_Y.Y) >= Math.Max(intervalA_Y.X, intervalB_Y.X))
            //{
            //    return true;
            //}
            if (recA.Intersects(recB)) return true;
            else return false;
        }

        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if(currentState == GameState.Playing)
            {
                // Checking collision between character and land
                for (int i = 0; i < landCount; i++)
                {
                    if (CollisionDetection(characterObject.spriteBox, landObject[i].spriteBox))
                    {
                        currentState = GameState.Stop;
                    }
                }

                // Checking collision between character and obstacle
                for (int i = 0; i < obstacleCount; i++)
                {
                    for (int j = 0; j < 2; j++)
                    {
                        if (CollisionDetection(characterObject.spriteBox, obstacleObject[i, j].spriteBox))
                        {
                            currentState = GameState.Stop;
                        }
                    }
                }

                if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                    Exit();

                for (int i = 0; i < backgroundCount; i++)
                {
                    backgroundObject[i].spriteDestination.X -= (int)(speedBackground * gameTime.ElapsedGameTime.TotalSeconds);
                }

                if (backgroundObject[currentBG].spriteDestination.X < offsetBG)
                {
                    int prevBG = currentBG - 1;
                    if (prevBG < 0) prevBG = backgroundCount - 1;
                    backgroundObject[prevBG].spriteDestination.X += backgroundCount * backgroundObject[prevBG].spriteDestination.Width;
                    currentBG = (currentBG + 1) % backgroundCount;
                }

                for (int i = 0; i < landCount; i++)
                {
                    landObject[i].spriteDestination.X -= (int)(speedLand * gameTime.ElapsedGameTime.TotalSeconds);
                    landObject[i].Update(gameTime);
                }

                if (landObject[currentLand].spriteDestination.X < offsetBG)
                {
                    int prevLand = currentLand - 1;
                    if (prevLand < 0) prevLand = landCount - 1;
                    landObject[prevLand].spriteDestination.X += landCount * landObject[prevLand].spriteDestination.Width;
                    currentLand = (currentLand + 1) % landCount;
                }

                speedCharacter += (float)(gravity * gameTime.ElapsedGameTime.TotalSeconds);
                characterObject.spriteDestination.Y = (int)(characterObject.spriteDestination.Y + speedCharacter * gameTime.ElapsedGameTime.TotalSeconds);
                characterObject.Update(gameTime);

                //    obstaclePosition.X -= (int)(obstacleSpeed * gameTime.ElapsedGameTime.TotalSeconds);
                //    obstacleBox.X = (int)(obstaclePosition.X);

                for (int i = 0; i < obstacleCount; i++)
                {
                    for (int j = 0; j < 2; j++)
                    {
                        obstacleObject[i, j].spriteDestination.X -= (int)(obstacleSpeed * gameTime.ElapsedGameTime.TotalSeconds);
                        obstacleObject[i, j].Update(gameTime);
                    }
                }

                if (obstacleObject[firstObstacleIndex, 0].spriteDestination.X < 0 - obstacleObject[firstObstacleIndex, 0].spriteDestination.Width)
                {
                    int randY = random.Next(150, screenHeight - 150);

                    obstacleObject[firstObstacleIndex, 0].spriteDestination.Y = randY;
                    obstacleObject[firstObstacleIndex, 1].spriteDestination.Y = randY - obstacleGap;

                    obstacleObject[firstObstacleIndex, 0].spriteDestination.X += obstacleCount * (obstacle.Width / 3 + obstacleDistance);
                    obstacleObject[firstObstacleIndex, 1].spriteDestination.X += obstacleCount * (obstacle.Width / 3 + obstacleDistance);

                    firstObstacleIndex = (firstObstacleIndex + 1) % obstacleCount;
                }

                if (Keyboard.GetState().IsKeyDown(Keys.Space))
                {
                    speedCharacter = speedOnJump;
                }

                //    if (speedCharacter < 0) whichCharacter = 0;
                //    else whichCharacter = 1;
                //}
            }

            if (currentState == GameState.Stop && Keyboard.GetState().IsKeyDown(Keys.R))
            {
                currentState = GameState.Playing;
                Initialize();
            }

            base.Update(gameTime);
        }

        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            // GraphicsDevice.Clear(Color.CornflowerBlue);

            Global.spriteBatch.Begin();

            for (int i = 0; i < backgroundCount; i++)
            {
                backgroundObject[i].Draw(gameTime);
            }

            characterObject.Draw(gameTime);

            for (int i = 0; i < obstacleCount; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    obstacleObject[i, j].Draw(gameTime);
                }
            }

            for (int i = 0; i < landCount; i++)
            {
                landObject[i].Draw(gameTime);
            }

            //for (int i = 0; i < obstacleCount; i++)
            //{
            //    Global.spriteBatch.Draw(
            //        intersectBox,
            //        obstacleObject[i, 1].spriteBox,
            //        Color.White);
            //}

            //spriteBatch.Draw(
            //    obstacle,
            //    new Rectangle((int)obstaclePosition.X, (int)obstaclePosition.Y, (int)obstacleDimension.X, (int)obstacleDimension.Y),
            //    obstacleSource,
            //    Color.White);

            Global.spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
